#ifndef BOOKDELETEWINDOW_H
#define BOOKDELETEWINDOW_H

#include <QDialog>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QStringList>
#include <QMessageBox>

namespace Ui {
class BookDeleteWindow;
}

class BookDeleteWindow : public QDialog
{
    Q_OBJECT
    QStringList stringList;
public:
    explicit BookDeleteWindow(QWidget *parent = 0);
    ~BookDeleteWindow();

private slots:
    void on_pushButton_delete_clicked();

private:
    Ui::BookDeleteWindow *ui;
};

#endif // BOOKDELETEWINDOW_H
