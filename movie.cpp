#include "movie.h"

//Movie::Movie()
//{

//}

Movie::Movie(int id, std::string f_title, int year, std::string f_author,
                std::string f_genre, int f_duration, std::string f_studio, std::__cxx11::string state/*="availalble"*/)
    :Item(id, f_title, year, f_author, f_genre, state)
{
    this->item_type = "Film";
    this->duration = f_duration;
    this->studio = f_studio;
}

std::string Movie::getItemData()
{
    std::stringstream ss;
    ss << item_type << '|' << item_id << '|' << title << '|' << release_year << '|' << author << '|' << genre
       << '|' << duration << '|' << studio <<  '|' << item_state;
    return ss.str();
}

void Movie::loadItemData(const std::string & Data)
{
    std::stringstream ss;
    ss << Data;
    ss >> item_type; ss >> item_id; ss >> title; ss >> release_year; ss >> author; ss >> genre;
    ss >> duration; ss >> studio; ss >> item_state;
}
