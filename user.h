#ifndef USER_H
#define USER_H
#include <iostream>
#include <string>
#include <sstream>
#include <array>
#include <memory>
#include "item.h"
#include <fstream>
#include <QFile>
#include <QTextStream>

class User
{
    friend class MainWindow;
    friend class ViewItemsWindow;
protected:
    std::string username;
    std::string password;
    std::string email;
    std::string user_type;
    std::array< std::shared_ptr<Item>, 3> books;
    std::array< std::string, 3> books_strings;
    std::string user_file;
public:
    User() = default;
    User(std::string user, std::string pass, std::string mail);
    virtual std::string getUserData();
    virtual void loadUserData(const std::string &Data);
    void borrowBook(std::string book);
    void saveBorrowedToFile();
    void replaceBorrowedInFile();
    void loadBorrowedFromFile();
    std::string getFileName();
    std::string getUserName();
    void removeBook(int index);
};

#endif // USER_H
