#ifndef BOOKADDWINDOW_H
#define BOOKADDWINDOW_H

#include <QDialog>

namespace Ui {
class BookAddWindow;
}

class BookAddWindow : public QDialog
{
    Q_OBJECT

public:
    explicit BookAddWindow(QWidget *parent = 0);
    ~BookAddWindow();
private slots:
    void itemBook();

    void itemMovie();

    void on_addBookButton_clicked();

    void on_returnButton_clicked();

private:
    Ui::BookAddWindow *ui;
};

#endif // BOOKADDWINDOW_H
