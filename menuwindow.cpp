#include "menuwindow.h"
#include "ui_menuwindow.h"
#include "QMessageBox"

menuwindow::menuwindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::menuwindow)
{
    ui->setupUi(this);
}

menuwindow::menuwindow(QString login, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::menuwindow)
{
    ui->setupUi(this);
    user_login = login;
}

menuwindow::~menuwindow()
{
    delete ui;
}

void menuwindow::on_pushButton_6_clicked() //PRZYCISK WYLOGOWANIA
{
    QMessageBox::StandardButton reply = QMessageBox::question(this, "Wylogowanie",
                   "Czy chcesz się wylogować?", QMessageBox::Yes | QMessageBox::No);
    if(reply== QMessageBox::Yes){
        QWidget* window = QWidget::parentWidget();
        window->show();
        this->close();
    }
}

void menuwindow::on_pushButton_2_clicked() //PRZYCISK MENU DODAWANIA POZYCJI
{
    bookmenu = new BookAddWindow(this);
    bookmenu->show();
}



void menuwindow::on_pushButton_clicked() //PRZYCISK MENU WYŚWIETLANIA POZYCJI
{
    viewmenu = new ViewItemsWindow(this);
    viewmenu->show();
}

void menuwindow::on_pushButton_4_clicked() //PRZYCISK MENU EDYCJI POZYCJI
{
    book_editmenu = new BookEditWindow(this);
    book_editmenu->show();
}

void menuwindow::on_pushButton_3_clicked() //PRZYCISK MENU USUWANIA POZYCJI
{
    deletemenu = new BookDeleteWindow(this);
    deletemenu->show();
}

void menuwindow::getLogin(QString login)
{
    user_login = login;
}

void menuwindow::on_pushButton_5_clicked()  //PRZYCISK MENU ODDAWANIA WYPOŻYCZONYCH POZYCJI
{
    borrowmenu = new BorrowedWindow(user_login, this);
    borrowmenu->show();
}
