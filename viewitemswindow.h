#ifndef VIEWITEMSWINDOW_H
#define VIEWITEMSWINDOW_H

#include "bookdeletewindow.h"
#include <QDialog>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QMessageBox>

namespace Ui {
class ViewItemsWindow;
}

class ViewItemsWindow : public QDialog
{
    Q_OBJECT
    QStringList stringList;
public:
    explicit ViewItemsWindow(QWidget *parent = 0);
    ~ViewItemsWindow();

private slots:
    void on_pushButton_load_clicked();

    void on_pushButton_delete_clicked();

    void on_pushButton_clicked();

    void on_listWidget_itemSelectionChanged();

    void on_pushButton_2_clicked();

private:
    Ui::ViewItemsWindow *ui;
    BookDeleteWindow *deletemenu;
};

#endif // VIEWITEMSWINDOW_H
