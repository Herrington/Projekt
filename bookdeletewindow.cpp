#include "bookdeletewindow.h"
#include "ui_bookdeletewindow.h"

BookDeleteWindow::BookDeleteWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BookDeleteWindow)
{
    ui->setupUi(this);
}

BookDeleteWindow::~BookDeleteWindow()
{
    delete ui;
}

void BookDeleteWindow::on_pushButton_delete_clicked()
{
    QFile file("BookStore.txt");                            //wczytujemy pozycje z pliku
    if(file.open(QIODevice::ReadOnly|QIODevice::Text))
    {

        QTextStream stream(&file);

        QString line;
        do {
            line = stream.readLine();
            stringList.append(line);                        //dodajemy kolejne linie(pozycje) do strinLista
            qDebug() << line;
        } while(!line.isNull());

        file.close();
    }

    QString index = ui->lineEdit->text();                   //zczytujemy numer indeksu podany przez użytkownika
    int id = index.toInt();
    stringList.removeAt(id-1);                              //usuwamy ze stringLista element na podanym indeksie

    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
      QMessageBox::warning(this, "Zapis", "Nie udało się wykonać zapisu");
    }
    else
    {
      QTextStream stream(&file);
      for (int i = 0; i < stringList.size(); ++i)           //zapisujemy pozostałe pozycje do pliku
        stream << stringList.at(i) << '\n';
    }
    file.close();
}
