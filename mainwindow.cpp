#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include "librarymanager.h"
#include "user.h"
#include "teacher.h"
#include "student.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_QuitButton_clicked()    //PRZYCISK WYJŚCIA W PROGRAMU
{
    QMessageBox::StandardButton reply = QMessageBox::question(this, "Exit the Program",
                   "Do you wish to exit the program?", QMessageBox::Yes | QMessageBox::No);
    if(reply== QMessageBox::Yes) QApplication::quit();
}

void MainWindow::on_LoginButton_clicked()   //PRZYCISK LOGOWANIA
{
    QString usernam = ui->Login_lineEdit->text();
    QString passwd = ui->Password_lineEdit->text();
    LibraryManager LibManager;
    LibManager.loadUsersFromFile("UsersData.txt");
    bool passed = false;                                        //zmienna do sprawdzania czy logowanie się powiodło

    for (size_t i = 0; i<LibManager.Userss.size(); i++)
    {
        if(usernam.toStdString() == LibManager.Userss[i]->username && passwd.toStdString() == LibManager.Userss[i]->password)
        {                                                   //sprawdzanie czy istnieje użytkownik o pasujących danych
            QMessageBox::information(this, "Login", "Logowanie powiodło się");
            hide();
            menu = new menuwindow(usernam, this);
            menu->show();
            passed = true;                                  //istnieje użytkownik o podanych danych, więc stawiamy wartość true
            emit loginAcquired(usernam);
        }
    }
    if(passed == false)
    {
        QMessageBox::warning(this, "Login", "Login lub hasło niewłaściwe");
    }
}

void MainWindow::on_pushButton_clicked()    //PRZYCISK REJESTRACJI
{
    regmenu = new registerwindow(this);
    regmenu->show();
}
