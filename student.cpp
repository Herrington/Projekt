#include "student.h"

Student::Student(std::__cxx11::string user, std::__cxx11::string pass, std::__cxx11::string mail, int id)
    :User(user, pass, mail)
{
    this->index = id;
    this->user_type = "Student";
}

std::string Student::getUserData()
{
    std::stringstream ss;
    ss << user_type << ' ' << username << ' ' << password << ' ' << email << ' ' << index << ' ' << user_file;
    return ss.str();
}

void Student::loadUserData(const std::string & Data)
{
    std::stringstream ss;
    ss << Data;
    ss >> user_type; ss >> username; ss >> password; ss >> email; ss >> index; ss >> user_file;
}
