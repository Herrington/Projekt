#include "book.h"

//Book::Book()
//{

//}

Book::Book(int id, std::string f_title, int year, std::string f_author, std::string f_genre, int page, std::string publish, std::__cxx11::string state/*="available"*/)
    :Item(id, f_title, year, f_author, f_genre, state)
{
    this->item_type = "Książka";
    this->pages = page;
    this->publisher = publish;
}


std::string Book::getItemData()
{
    std::stringstream ss;
    ss << item_type << '|' << item_id << '|' << title << '|' << release_year << '|' << author << '|' << genre
       << '|' << pages << '|' << publisher <<  '|' << item_state;
    return ss.str();
}

void Book::loadItemData(const std::string & Data)
{
    std::stringstream ss;
    ss << Data;
    ss >> item_type; ss >> item_id; ss >> title; ss >> release_year; ss >> author; ss >> genre;
    ss >> pages; ss >> publisher; ss >> item_state;
}
