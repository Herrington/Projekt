#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <memory>
#include "menuwindow.h"
#include <iostream>
#include "registerwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_QuitButton_clicked();

    void on_LoginButton_clicked();

    void on_pushButton_clicked();

signals:
    void loginAcquired(QString login);

private:
    Ui::MainWindow *ui;
    menuwindow *menu;
    registerwindow *regmenu;
};

#endif // MAINWINDOW_H
