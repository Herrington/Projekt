#ifndef TEACHER_H
#define TEACHER_H
#include "user.h"
#include <QString>
#include <iostream>
#include <string>

class Teacher : public User
{
private:
    std::string teacher_id;
public:
    Teacher() = default;
    Teacher(std::string user, std::string pass, std::string mail, std::string teachr_id);
    virtual std::string getUserData();
    virtual void loadUserData(const std::string &Data);
};

#endif // TEACHER_H
