#ifndef BORROWEDWINDOW_H
#define BORROWEDWINDOW_H

#include <QDialog>
#include "librarymanager.h"
#include <QStringList>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QMessageBox>

namespace Ui {
class BorrowedWindow;
}

class BorrowedWindow : public QDialog
{
    Q_OBJECT
    QString user_login;
    QStringList stringList;

public:
    explicit BorrowedWindow(QWidget *parent = 0);
    BorrowedWindow(QString login, QWidget *parent);
    ~BorrowedWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::BorrowedWindow *ui;
};

#endif // BORROWEDWINDOW_H
