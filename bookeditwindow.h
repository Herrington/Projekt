#ifndef BOOKEDITWINDOW_H
#define BOOKEDITWINDOW_H

#include <QDialog>
#include <QLineEdit>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QStringList>
#include <QMessageBox>

namespace Ui {
class BookEditWindow;
}

class BookEditWindow : public QDialog
{
    Q_OBJECT
    QStringList stringList;
public:
    explicit BookEditWindow(QWidget *parent = 0);
    ~BookEditWindow();

private slots:
    void on_pushButton_clicked();

    void itemToEdit();

    void on_addBookButton_clicked();

private:
    Ui::BookEditWindow *ui;
};

#endif // BOOKEDITWINDOW_H
