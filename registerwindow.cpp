#include "registerwindow.h"
#include "ui_registerwindow.h"
#include <QMessageBox>
#include "user.h"
#include "student.h"
#include "teacher.h"
#include "librarymanager.h"

registerwindow::registerwindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::registerwindow)
{
    ui->setupUi(this);
    ui->user_comboBox->addItem("");
    ui->user_comboBox->addItem("Student");
    ui->user_comboBox->addItem("Teacher");

    //Łączenie sygnałów ze slotami by umożliwić odblokowanie i blokowanie odpowiednich lineEditów
    connect(ui->user_comboBox, SIGNAL (currentTextChanged(QString)), this, SLOT (userTeacher()));
    connect(ui->user_comboBox, SIGNAL (currentTextChanged(QString)), this, SLOT (userStudent()));
}
registerwindow::~registerwindow()
{
    delete ui;
}

void registerwindow::on_pushButton_clicked() //METODA PRZYCISKU DO REJESTRACJI
{
    QString username = ui->lineEdit_username->text();
    QString password = ui->lineEdit_passwd->text();
    QString password_2 = ui->lineEdit_passwd_2->text();
    QString email = ui->lineEdit_email->text();
    QString email_2 = ui->lineEdit_email_2->text();
    QString index = ui->lineEdit_index->text();
    QString teacher_id = ui->lineEdit_tchr_id->text();
    static LibraryManager *LibManager = new LibraryManager();

    if((ui->lineEdit_passwd->text()==ui->lineEdit_passwd_2->text())&&ui->lineEdit_passwd->text()!="") //sprawdzanie czy podane hasła się zgadzają
    {

        if((ui->lineEdit_email->text()==ui->lineEdit_email_2->text())&&ui->lineEdit_email->text()!="") //sprawdzanie czy maile sie zgadzaja
        {
            //w zależności od wyboru w comboBoxie tworzenie ucznia/nauczyciela
            if(ui->user_comboBox->currentText()=="Student")
            {
                LibManager->createStudent(username, password, email, index);
            }
            else if(ui->user_comboBox->currentText()=="Teacher")
            {
                LibManager->createTeacher(username, password, email, teacher_id);
            }

            LibManager->saveUsersToFile("UsersData.txt");
            delete LibManager;
            QMessageBox::information(this, "Nowy użytkownik", "Utworzono nowego użytkownika");
        }
        else QMessageBox::warning(this, "E-mail", "Podane adresy e-mail różnią się!");
    }
    else QMessageBox::warning(this, "Hasło", "Podane hasła różnią się!");
}

void registerwindow::userTeacher() //SLOT ODBLOKOWUJĄCY LINE_EDITY DLA NAUCZYCIELA
{
    QString user_type = ui->user_comboBox->currentText();
    if (user_type.compare("Teacher")==0) {                      //jeśli wybrano nauczyciela to blokowane jest pole da ucznia
        ui->lineEdit_index->setDisabled(true);
        ui->lineEdit_tchr_id->setDisabled(false);
    }
}

void registerwindow::userStudent() //SLOT ODBLOKOWUJĄCY LINE_EDITY DLA UCZNIA
{
    QString user_type = ui->user_comboBox->currentText();
    if (user_type.compare("Student")==0) {
        ui->lineEdit_tchr_id->setDisabled(true);                //jeśli wybrano ucznia to blokowane jest pole dla nauczyciela
        ui->lineEdit_index->setDisabled(false);
    }
}

void registerwindow::on_pushButton_2_clicked() //PRZYCISK POWROTU DO EKRANU GŁÓWNEGO
{
    QMessageBox::StandardButton reply = QMessageBox::question(this, "Powrót do ekranu głównego",
                   "Czy chcesz porzucić rejestrację?", QMessageBox::Yes | QMessageBox::No);
    if(reply== QMessageBox::Yes){
        this->close();
    }
}
