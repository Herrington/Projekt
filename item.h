#ifndef ITEM_H
#define ITEM_H
#include <iostream>
#include <string>
#include <sstream>
#include <limits>
#include <QString>

class Item
{
    friend class BookAddWindow;
protected:
    int item_id;
    std::string title;
    int release_year;
    std::string author;
    std::string genre;
    std::string item_state;
    std::string item_type;
public:
    Item() = default;
    Item(int id, std::string f_title, int year, std::string f_author, std::string f_genre, std::string f_state);
    virtual std::string getItemData();
    virtual void loadItemData(const std::string &Data);
    virtual QString dataToDisplay();
    std::string getItemState();
};

#endif // ITEM_H
