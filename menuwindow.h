#ifndef MENUWINDOW_H
#define MENUWINDOW_H

#include <QDialog>
#include "bookaddwindow.h"
#include "viewitemswindow.h"
#include "bookeditwindow.h"
#include "bookdeletewindow.h"
#include "borrowedwindow.h"
#include <QPixmap>

namespace Ui {
class menuwindow;
}

class menuwindow : public QDialog
{
    friend class BookDeleteWindow;
    friend class BorrowedWindow;

    Q_OBJECT
    QString user_login;

public:
    explicit menuwindow(QWidget *parent = 0);
    menuwindow(QString login, QWidget *parent);
    ~menuwindow();

private slots:
    void on_pushButton_6_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

    void getLogin(QString login);

    void on_pushButton_5_clicked();

private:
    Ui::menuwindow *ui;
    BookAddWindow *bookmenu;
    ViewItemsWindow *viewmenu;
    BookEditWindow *book_editmenu;
    BookDeleteWindow *deletemenu;
    BorrowedWindow  *borrowmenu;
};

#endif // MENUWINDOW_H
