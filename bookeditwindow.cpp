#include "bookeditwindow.h"
#include "ui_bookeditwindow.h"

BookEditWindow::BookEditWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BookEditWindow)
{
    ui->setupUi(this);
    ui->comboBox_item->addItem("-");
    ui->comboBox_item->addItem("Książka");
    ui->comboBox_item->addItem("Film");
    ui->comboBox->addItem("-");

    //WCZYTYWANIE POZYCJI BIBLIOTECZNYCH Z PLIKU
    QFile file("BookStore.txt");
    QTextStream stream(&file);
    if(file.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        QString line;
        do {
            line = stream.readLine();
            stringList.append(line);
            qDebug() << line;
        } while(!line.isNull());
        unsigned int i=0;
        foreach(QString item, stringList) //zliczanie elementów stringLista
        {
            i++;
        }
        for(unsigned int j = 1; j<i; j++)
        {
            ui->comboBox->addItem(QString::number(j)); //dodawanie numerów indeksów do comboBoxa
        }

        file.close();
    }
    connect(ui->comboBox, SIGNAL (currentTextChanged(QString)), this, SLOT (itemToEdit()));
}

BookEditWindow::~BookEditWindow()
{
    delete ui;
}

void BookEditWindow::itemToEdit() //SLOT WCZYTUJACY WYBRANY DO EDYCJI REKORD
{
    QString index = ui->comboBox->currentText();
    int id = index.toInt();
    QString line = stringList[id-1];
    QStringList List = line.split("|");           //dzielimy stringa z pozycją biblioteczną na poszczególne pola,
    ui->lineEdit_title->setText(List[2]);         //a nastepnie wczytujemy zawartości do lineEditów
    ui->lineEdit_releaseyear->setText(List[3]);
    ui->lineEdit_author->setText(List[4]);
    ui->lineEdit_genre->setText(List[5]);
    ui->lineEdit_pages->setText(List[6]);
    ui->lineEdit_publisher->setText(List[7]);
    ui->lineEdit_state->setText(List[8]);
}

void BookEditWindow::on_pushButton_clicked() //PRZYCISK ŁADOWANIA POZYCJI DO EDYTOWANIA
{
    QString index = ui->comboBox->currentText();
    int id = index.toInt();
    QStringList listAfter;
    QString item_type = ui->comboBox_item->currentText();
    QString title = ui->lineEdit_title->text();
    QString year = ui->lineEdit_releaseyear->text();
    QString author = ui->lineEdit_author->text();
    QString genre = ui->lineEdit_genre->text();
    QString pages_duration = ui->lineEdit_pages->text();
    QString publish_studio = ui->lineEdit_publisher->text();
    QString state = ui->lineEdit_state->text();

    listAfter.append(item_type);                    //po wprowadzeniu zmienionych wartości wczytujemy je z powrotem
    listAfter.append(index);                        //i dodajemy do QStringLista
    listAfter.append(title);
    listAfter.append(year);
    listAfter.append(author);
    listAfter.append(genre);
    listAfter.append(pages_duration);
    listAfter.append(publish_studio);
    listAfter.append(state);

    QString after = listAfter.join("|");            //łaczymy poszczególne pola w jednego QStringa
    stringList.removeAt(id-1);                      //usuwamy pozycję sprzed edycji i wstawiamy już edytowaną
    stringList.insert(id-1, after);

    QFile file("BookStore.txt");
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
      QMessageBox::warning(this, "Zapis", "Nie udało się wykonać zapisu");
    }
    else
    {
      QTextStream stream(&file);
      int i;
      for (i=0; i < stringList.size(); i++)
        if(i < stringList.size()-1) stream << stringList.at(i) << '\n';     //wczytujemy pozycje z powrotem do pliku
        else stream << stringList.at(i);
    }
    file.close();
}

void BookEditWindow::on_addBookButton_clicked()
{

}
