#include "bookaddwindow.h"
#include "ui_bookaddwindow.h"
#include "item.h"
#include "book.h"
#include "movie.h"
#include "librarymanager.h"
#include <QMessageBox>

BookAddWindow::BookAddWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BookAddWindow)
{
    ui->setupUi(this);
    ui->comboBox_item->addItem("");
    ui->comboBox_item->addItem("Książka");
    ui->comboBox_item->addItem("Film");

    //Łączenie sygnałów ze slotami by umożliwić odblokowanie i blokowanie odpowiednich lineEditów
    connect(ui->comboBox_item, SIGNAL (currentTextChanged(QString)), this, SLOT (itemBook()));
    connect(ui->comboBox_item, SIGNAL (currentTextChanged(QString)), this, SLOT (itemMovie()));
}

BookAddWindow::~BookAddWindow()
{
    delete ui;
}

void BookAddWindow::itemBook()
{
    QString item_type = ui->comboBox_item->currentText();
    if (item_type.compare("Książka")==0) {                  //Jeśli wybrano książkę jako typ pozycji
        ui->lineEdit_studio->setDisabled(true);             //to blokowane są pola dla filmu, a pola dla książki odblokowane
        ui->lineEdit_duration->setDisabled(true);
        ui->lineEdit_pages->setDisabled(false);
        ui->lineEdit_publisher->setDisabled(false);
    }
}

void BookAddWindow::itemMovie(){
    QString item_type = ui->comboBox_item->currentText();
    if (item_type.compare("Film")==0) {                     //Jeśli wybrano film jako typ pozycji
        ui->lineEdit_pages->setDisabled(true);              //to blokowane są pola dla książki, a pola dla filmu odblokowane
        ui->lineEdit_publisher->setDisabled(true);
        ui->lineEdit_studio->setDisabled(false);
        ui->lineEdit_duration->setDisabled(false);
    }
}

void BookAddWindow::on_addBookButton_clicked()
{
    QString ID = ui->lineEdit_ID->text();
    QString title = ui->lineEdit_title->text();
    QString release_date = ui->lineEdit_releaseyear->text();
    QString author = ui->lineEdit_author->text();
    QString genre = ui->lineEdit_genre->text();
    QString studio = ui->lineEdit_studio->text();
    QString duration = ui->lineEdit_duration->text();
    QString pages = ui->lineEdit_pages->text();
    QString publisher = ui->lineEdit_publisher->text();             //zapisywanie wprowadzonych danych do QString

    LibraryManager *LibrManager = new LibraryManager();
    LibrManager->loadItemsFromFile("BookStore.txt");

    unsigned int error = 0;
    for(size_t i=0; i<LibrManager->Items.size(); i++)
    {                                                                          //sprawdzanie czy nie istnieje pozycja
        if(ID.toInt() == LibrManager->Items[i]->item_id) error++;              //o wybranym przez nas indeksie,
    }                                                                          //jeśli istnieje to zwiekszamy wartość zmiennej error
    if(ui->comboBox_item->currentText()=="Książka")
    {
        LibrManager->createBook(ID, title, release_date, author, genre, pages, publisher);      //jeśli wybrany typ to książka to tworzymy książkę
    }
    else if(ui->comboBox_item->currentText()=="Film")
    {
        LibrManager->createMovie(ID, title, release_date, author, genre, duration, studio);     //analogicznie dla filmu
    }
    if(error>0)
    {
        QMessageBox::critical(this, "Błąd", "Pozycja o podanym indeksie już istnieje!");        //jeśli error ma wartość wieksza niz 0 to wyrzucamy błąd
    }
    else
    {
        LibrManager->saveItemsToFile("BookStore.txt");                                          //zapis pozycji do pliku
        QMessageBox::information(this, "Nowa pozycja", "Dodano nową pozycję do biblioteki");
        LibrManager->Userss.clear();
        delete LibrManager;
        this->close();
        error = 0;
    }
}

void BookAddWindow::on_returnButton_clicked()
{
    QMessageBox::StandardButton reply = QMessageBox::question(this, "Powrót do menu",
                   "Czy chcesz przerwać dodawanie pozycji?", QMessageBox::Yes | QMessageBox::No);
    if(reply== QMessageBox::Yes){
        this->close();
    }
}
