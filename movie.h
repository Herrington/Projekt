#ifndef MOVIE_H
#define MOVIE_H
#include "item.h"
#include <iostream>
#include <sstream>

class Movie : public Item
{
private:
    int duration;
    std::string studio;
public:
    Movie() = default;
    Movie(int id, std::string f_title, int year, std::string f_author,
          std::string f_genre, int f_duration, std::string f_studio, std::string state="available");
    virtual std::string getItemData();
    virtual void loadItemData(const std::string &Data);
};

#endif // MOVIE_H
