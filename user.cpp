#include "user.h"

User::User(std::__cxx11::string user, std::__cxx11::string pass, std::__cxx11::string mail)
{
    this->username = user;
    this->password = pass;
    this->email = mail;
    this->user_file = user+".txt";
    this->books_strings = {"", "", ""};
}

void createUser(){
}

std::string User::getUserData()
{
    return std::string();
}

void User::loadUserData(const std::string & Data)
{
}

void User::borrowBook(std::__cxx11::string book)
{
    for(unsigned int i = 0; i<books_strings.size(); i++)
    {
        if(books_strings[i] == ""){
            books_strings[i] = book;
            break;
        }
        else continue;
    }
}

void User::saveBorrowedToFile() //ZAPISYWANIE WYPOŻYCZONYCH KSIĄŻEK DO PLIKU
{
    std::fstream File;
    File.open(user_file, std::ios::out | std::ios::app);
    if (File.good())
    {
        for (size_t i = 0; i<books_strings.size(); i++)
        {
            if(books_strings[i] != "")
            {
            File << books_strings[i];
            File << std::endl;
            }
        }
        File.close();
    }
}

void User::replaceBorrowedInFile() //ZAPISYWANIE WYPOŻYCZONYCH KSIĄŻEK DO PLIKU (ZASTĘPUJĄC OBECNE)
{
    std::fstream File;
    File.open(user_file, std::ios::out);
    if (File.good())
    {
        for (size_t i = 0; i<books_strings.size(); i++)
        {
            if(books_strings[i] != "")
            {
            File << books_strings[i];
            File << std::endl;
            }
        }
        File.close();
    }
}

void User::loadBorrowedFromFile() //WCZYTYWANIE WYPOŻYCZONYCH KSIĄŻEK Z PLIKU
{
    std::fstream File;
    File.open(user_file, std::ios::in);
    if (File.good())
    {
        for(unsigned int i = 0; i<books_strings.size(); i++)
        {
            std::getline(File, books_strings[i]);
        }
    }
}

std::string User::getFileName()
{
    return user_file;
}

std::string User::getUserName()
{
    return username;
}

void User::removeBook(int index) //USUWANIE WYPOZYCZONEJ KSIĄŻKI Z ARRAY
{
    books_strings[index] = "";
}
