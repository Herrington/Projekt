#include "teacher.h"

Teacher::Teacher(std::__cxx11::string user, std::__cxx11::string pass, std::__cxx11::string mail, std::__cxx11::string teachr_id)
    :User(user, pass, mail)
{
    this->teacher_id = teachr_id;
    this->user_type = "Teacher";
}

std::string Teacher::getUserData()
{
    std::stringstream ss;
    ss << user_type << ' ' << username << ' ' << password << ' ' << email << ' ' << teacher_id << ' ' << user_file;
    return ss.str();
}

void Teacher::loadUserData(const std::string & Data)
{
    std::stringstream ss;
    ss << Data;
    ss >> user_type; ss >> username; ss >> password; ss >> email; ss >> teacher_id; ss>> user_file;
}
