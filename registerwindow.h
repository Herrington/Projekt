#ifndef REGISTERWINDOW_H
#define REGISTERWINDOW_H

#include <QDialog>
#include <iostream>
#include <string>


namespace Ui {
class registerwindow;
}

class registerwindow : public QDialog
{
    Q_OBJECT

public:
    explicit registerwindow(QWidget *parent = 0);
    ~registerwindow();

private slots:
    void on_pushButton_clicked();

    void userTeacher();

    void userStudent();

    void on_pushButton_2_clicked();

private:
    Ui::registerwindow *ui;
};

#endif // REGISTERWINDOW_H
