#include "mainwindow.h"
#include <QApplication>
#include "librarymanager.h"
#include "user.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    LibraryManager LibManager;

    return a.exec();
}
