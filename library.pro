#-------------------------------------------------
#
# Project created by QtCreator 2017-05-27T12:42:41
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = library
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    librarymanager.cpp \
    user.cpp \
    student.cpp \
    teacher.cpp \
    item.cpp \
    book.cpp \
    movie.cpp \
    menuwindow.cpp \
    registerwindow.cpp \
    bookaddwindow.cpp \
    viewitemswindow.cpp \
    bookeditwindow.cpp \
    bookdeletewindow.cpp \
    borrowedwindow.cpp

HEADERS  += mainwindow.h \
    librarymanager.h \
    user.h \
    student.h \
    teacher.h \
    item.h \
    book.h \
    movie.h \
    menuwindow.h \
    registerwindow.h \
    bookaddwindow.h \
    viewitemswindow.h \
    bookeditwindow.h \
    bookdeletewindow.h \
    borrowedwindow.h

FORMS    += mainwindow.ui \
    menuwindow.ui \
    registerwindow.ui \
    bookaddwindow.ui \
    viewitemswindow.ui \
    bookeditwindow.ui \
    bookdeletewindow.ui \
    borrowedwindow.ui

RESOURCES += \
    resource.qrc
