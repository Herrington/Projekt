#include "borrowedwindow.h"
#include "ui_borrowedwindow.h"

BorrowedWindow::BorrowedWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BorrowedWindow)
{
    ui->setupUi(this);
}

BorrowedWindow::BorrowedWindow(QString login, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BorrowedWindow)
{
    ui->setupUi(this);
    user_login = login;
}

BorrowedWindow::~BorrowedWindow()
{
    delete ui;
}

void BorrowedWindow::on_pushButton_clicked()
{
    LibraryManager LibManager;
    LibManager.loadUsersFromFile("UsersData.txt");

    for (size_t i = 0; i<LibManager.Userss.size(); i++)
    {
        if(user_login.toStdString() == LibManager.Userss[i]->getUserName())
        {
            std::string file_name = LibManager.Userss[i]->getFileName();        //sprawdzamy imię zalogowanego użytkownika
            QString filenam = QString::fromStdString(file_name);                //żeby wczytać jego plik z wypożyczonymi pozycjami
            QFile file(filenam);
            if(file.open(QIODevice::ReadOnly|QIODevice::Text))                  //wczytujemy plik
            {

                QTextStream stream(&file);

                QString line;
                do {
                    line = stream.readLine();
                    stringList.append(line);
                    qDebug() << line;
                } while(!line.isNull());

                foreach(QString item, stringList)
                {
                    ui->listWidget->addItem(item);                              //dodajemy wczytane książki do listWidgeta
                }

                file.close();
            }
        }
    }

}

void BorrowedWindow::on_pushButton_2_clicked()
{
    LibraryManager LibManager;
    LibManager.loadUsersFromFile("UsersData.txt");

    int row = ui->listWidget->currentRow();
    for (size_t i = 0; i<LibManager.Userss.size(); i++)
    {
        if(user_login.toStdString() == LibManager.Userss[i]->getUserName())     //szukamy właściwego użytkownika
        {   
            LibManager.Userss[i]->loadBorrowedFromFile();                       //ładujemy jego książki

            QString borrowed = stringList[row];
            qDebug() << borrowed;
            QStringList bookAfter = borrowed.split("|");

            QStringList booksBefore;
            QFile file("BookStore.txt");
            if(file.open(QIODevice::ReadOnly|QIODevice::Text))
            {

                QTextStream stream(&file);

                QString line;
                do {
                    line = stream.readLine();
                    booksBefore.append(line);
                    qDebug() << line;
                } while(!line.isNull());

                file.close();
            }
            for(int i = 0; i < booksBefore.size()-1; i++)
            {
                QString line = booksBefore[i];
                QStringList lineList = line.split("|");
                qDebug() << line;
                if(lineList[1] == bookAfter[1] && lineList[2] == bookAfter[2])
                {
                    lineList[8] = "available";
                    QString line_after = lineList.join("|");
                    booksBefore[i] = line_after;
                }
            }
            if (!file.open(QFile::WriteOnly | QFile::Text))
            {
              QMessageBox::warning(this, "Zapis", "Nie udało się wykonać zapisu");
            }
            else
            {
              QTextStream stream(&file);
              for (int i=0; i < booksBefore.size(); i++)
                if(i < booksBefore.size()-1) stream << booksBefore.at(i) << '\n';     //wczytujemy pozycje z powrotem do pliku
                else stream << booksBefore.at(i);
            }
            file.close();

            LibManager.Userss[i]->removeBook(row);                              //usuwamy odpowiednią pozycję
            LibManager.Userss[i]->replaceBorrowedInFile();                      //zapisujemy pozostałe pozycje (bez usuniętej)
        }
    }
}
