#ifndef LIBRARYMANAGER_H
#define LIBRARYMANAGER_H
#include <vector>
#include <iostream>
#include "user.h"
#include <QString>
#include <string>
#include <memory>
#include "student.h"
#include "teacher.h"
#include <fstream>
#include <sstream>
#include <item.h>
#include <book.h>
#include <movie.h>
#include <QList>
#include <QHash>

class LibraryManager
{
    friend class MainWindow;
    friend class BookAddWindow;
    friend class ViewItemsWindow;
    friend class BorrowedWindow;
protected:
    std::vector<std::shared_ptr<User> > Userss; //wektor użytkowników
    std::vector<std::shared_ptr<Item> > Items; //wektor pozycji bibliotecznych
    QHash<int, std::shared_ptr<Item> > lib_items;
    QStringList stringList;
public:
    explicit LibraryManager();
    ~LibraryManager() = default;
    void saveUsersToFile(std::string filePath); //zapis użytkowników do pliku
    std::vector<std::shared_ptr<User> > loadUsersFromFile(std::string filePath); //ładowanie użytkowników z pliku
    void saveItemsToFile(std::string filePath);
    std::vector<std::shared_ptr<Item> > loadItemsFromFile(std::string filePath);

    std::shared_ptr<User> createStudent(QString username, QString passwd, QString email, QString indx); //tworzenie ucznia
    std::shared_ptr<User> createTeacher(QString username, QString passwd, QString email, QString teach_id); //tworzenie nauczyciela

    std::shared_ptr<Item> createBook(QString item_id, QString title, QString release_date, QString author,
                                        QString genre, QString pages, QString publisher); //tworzenie książki

    std::shared_ptr<Item> createMovie(QString item_id, QString title, QString release_date, QString author,
                                        QString genre, QString duration, QString studio); //tworzenie filmu
};

#endif // LIBRARYMANAGER_H
