#ifndef BOOK_H
#define BOOK_H
#include "item.h"
#include <iostream>
#include <string>
#include <sstream>

class Book : public Item
{
private:
    int pages;
    std::string publisher;
public:
    Book() = default;
    Book(int id, std::string f_title, int year, std::string f_author,
         std::string f_genre, int page, std::string publish, std::string state="available");
    virtual std::string getItemData();
    virtual void loadItemData(const std::string &Data);
};

#endif // BOOK_H
