#ifndef STUDENT_H
#define STUDENT_H
#include "user.h"
#include <string>
#include <QString>

class Student :public User
{
private:
    int index;
public:
    Student() = default;
    Student(std::string user, std::string pass, std::string mail, int id);
    virtual std::string getUserData();
    virtual void loadUserData(const std::string &Data);
};

#endif // STUDENT_H
