#include "librarymanager.h"

LibraryManager::LibraryManager()
{
}

std::shared_ptr<User> LibraryManager::createStudent(QString username, QString passwd, QString email, QString indx) //TWORZENIE UCZNIA
{

    std::string user = username.toStdString();
    std::string pass = passwd.toStdString();
    std::string mail = email.toStdString();
    int id = indx.toInt();

    std::shared_ptr<User> student(new Student(user, pass, mail, id));
    Userss.push_back(student);

    return student;
}

std::shared_ptr<User> LibraryManager::createTeacher(QString username, QString passwd, QString email, QString teach_id) //TWORZENIE NAUCZYCIELA
{

    std::string user = username.toStdString();
    std::string pass = passwd.toStdString();
    std::string mail = email.toStdString();
    std::string teachr_id = teach_id.toStdString();

    std::shared_ptr<User> teacher(new Teacher(user, pass, mail, teachr_id));
    Userss.push_back(teacher);

    return std::move(teacher);
}

void LibraryManager::saveUsersToFile(std::string filePath) //ZAPIS UŻYTKOWNIKÓW DO PLIKU
{
    std::fstream File;
    File.open(filePath, std::ios::out | std::ios::app);
    if (File.good())
    {
        for (size_t i = 0; i<Userss.size(); ++i)
        {
            File << Userss[i]->getUserData();
            File << std::endl;
        }
        File.close();
    }
}

std::vector<std::shared_ptr<User> > LibraryManager::loadUsersFromFile(std::string filePath) //WCZYTYWANIE UŻYTKOWNIKÓW Z PLIKU
{
    std::fstream File;
    std::string Line;
    File.open(filePath, std::ios::in);

    if (File.good())
    {
        while (std::getline(File, Line))
        {
            std::string user_type;
            std::stringstream ss;
            ss << Line; ss >> user_type;
            if (user_type == "Student")
            {
                std::shared_ptr<User> student(new Student());
                Userss.push_back(student);
            }
            else if (user_type == "Teacher")
            {
                std::shared_ptr<User> teacher(new Teacher());
                Userss.push_back(teacher);
            }
            if (user_type == "Student" || user_type == "Teacher")
            {
                Userss.back()->loadUserData(Line);
            }
        }
        File.close();
    }
    return Userss;
}

std::shared_ptr<Item> LibraryManager::createBook(QString item_id, QString title, QString release_date,
                                                 QString author, QString genre, QString pages, QString publisher)
{                                                                                           //TWORZENIE KSIĄŻKI
    int f_id = item_id.toInt();
    std::string f_title = title.toStdString();
    int f_release = release_date.toInt();
    std::string f_author = author.toStdString();
    std::string f_genre = genre.toStdString();
    int f_pages = pages.toInt();
    std::string f_publisher = publisher.toStdString();

    std::shared_ptr<Item> book(new Book(f_id, f_title, f_release, f_author, f_genre, f_pages, f_publisher));
    Items.push_back(book);


    return book;
}

std::shared_ptr<Item> LibraryManager::createMovie(QString item_id, QString title, QString release_date,
                                                  QString author, QString genre, QString duration, QString studio)
{                                                                                           //TWORZENIE FILMU
    int f_id = item_id.toInt();
    std::string f_title = title.toStdString();
    int f_release = release_date.toInt();
    std::string f_author = author.toStdString();
    std::string f_genre = genre.toStdString();
    int f_duration = duration.toInt();
    std::string f_studio = studio.toStdString();

    std::shared_ptr<Item> movie(new Movie(f_id, f_title, f_release, f_author, f_genre, f_duration, f_studio));
    Items.push_back(movie);


    return movie;
}

void LibraryManager::saveItemsToFile(std::string filePath)  //ZAPIS POZYCJI BIBLIOTECZNYCH DO PLIKU
{
    std::fstream File;
    File.open(filePath, std::ios::out | std::ios::app);
    if (File.good())
    {
        for (size_t i = 0; i<Items.size(); ++i)
        {
            File << Items[i]->getItemData();
            File << std::endl;
        }
        File.close();
    }
}

std::vector<std::shared_ptr<Item> > LibraryManager::loadItemsFromFile(std::string filePath) //WCZYTYWANIE POZYCJI Z PLIKU
{
    std::fstream File;
    std::string Line;
    File.open(filePath, std::ios::in);

    if (File.good())
    {
        while (std::getline(File, Line))
        {
            std::string item_type;
            std::stringstream ss;
            ss << Line; ss >> item_type;
            if (item_type == "Książka")
            {
                std::shared_ptr<Item> book(new Book());
                Items.push_back(book);
            }
            else if (item_type == "Film")
            {
                std::shared_ptr<Item> movie(new Movie());
                Items.push_back(movie);
            }
            if (item_type == "Książka" || item_type == "Film")
            {
                Items.back()->loadItemData(Line);
            }
        }
        File.close();
    }
    return Items;
}
