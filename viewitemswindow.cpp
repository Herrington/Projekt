#include "viewitemswindow.h"
#include "ui_viewitemswindow.h"
#include "librarymanager.h"

ViewItemsWindow::ViewItemsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ViewItemsWindow)
{
    ui->setupUi(this);
}

ViewItemsWindow::~ViewItemsWindow()
{
    delete ui;
}

void ViewItemsWindow::on_pushButton_load_clicked()  //ŁADOWANIE POZYCJI Z PLIKU
{
    QFile file("BookStore.txt");
    if(file.open(QIODevice::ReadOnly|QIODevice::Text))
    {

        QTextStream stream(&file);

        QString line;
        do {
            line = stream.readLine();
            stringList.append(line);
            qDebug() << line;
        } while(!line.isNull());
        int i = 1;
        foreach(QString item, stringList)
        {
            i++;
            QString id = QString::number(i);
            ui->listWidget->addItem("Id."+id+" "+item); //dodawanie pozycji bibliotecznych do listWidget z dodaniem indeksowania
        }

        file.close();
    }
}

void ViewItemsWindow::on_pushButton_delete_clicked()
{
    deletemenu = new BookDeleteWindow(this);
    deletemenu->show();
}

void ViewItemsWindow::on_pushButton_clicked()
{
    ui->lineEdit_verify->setEnabled(true);
    QMessageBox::information(this, "Wypożyczenie", "Podaj swoje hasło i potwierdź wypożyczenie");
}


void ViewItemsWindow::on_listWidget_itemSelectionChanged()
{
    ui->pushButton->setEnabled(true);
}

void ViewItemsWindow::on_pushButton_2_clicked()
{
    QString position = ui->listWidget->currentItem()->text();
    QString passwd = ui->lineEdit_verify->text();
    std::string book = position.toStdString();
    std::size_t found = book.find("available");
    int row = ui->listWidget->currentRow();
    LibraryManager LibManager;
    LibManager.loadUsersFromFile("UsersData.txt");
    bool passed = false;

    for (size_t i = 0; i<LibManager.Userss.size(); i++)
    {
        if(passwd.toStdString() == LibManager.Userss[i]->password)  //weryfikacja użytkownika celem wypożyczenia
        {
            if(found != std::string::npos)
            {
            LibManager.Userss[i]->borrowBook(book);                 //przypisanie książki do użytkownika
            LibManager.Userss[i]->saveBorrowedToFile();             //zapisanie wypożyczonych książek do pliku użytkownika
            QMessageBox::information(this, "Wypożyczenie", "Pozycja wypożyczona");
            passed = true;

            QString borrowed = stringList[row];
            QStringList bookAfter = borrowed.split("|");
            bookAfter.replace(8, "taken");
            QString after = bookAfter.join("|");
            stringList.removeAt(row);
            stringList.insert(row, after);

            QFile file("BookStore.txt");
            if (!file.open(QFile::WriteOnly | QFile::Text))
            {
              QMessageBox::warning(this, "Zapis", "Nie udało się wykonać zapisu");
            }
            else
            {
              QTextStream stream(&file);
              int i;
              for (i=0; i < stringList.size(); i++)
                if(i < stringList.size()-1) stream << stringList.at(i) << '\n';     //wczytujemy pozycje z powrotem do pliku
                else stream << stringList.at(i);
            }
            file.close();
            }
        }
    }
    if(passed == false)
    {
        QMessageBox::warning(this, "Weryfikacja", "Hasło niewłaściwe");
    }
}
